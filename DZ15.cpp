#include <iostream>

using namespace std;

void const EvenNumbers(int N = 20)
{
    cout << "Even: ";
    for (int i = 0; i < 20; i++) {

        if (i % 2 == 0)
            cout << i << " ";
    }
}

void const OddNumbers(int N = 20)
{
    cout << "\nOdd: ";
    for (int i = 1; i < 20; i++) {
        if (i % 2 != 0)
            cout << i << " ";
    }
}


int main()
{
    int N = 0;
    std::cout << "Output even numbers? (y/n): ";
    char answer = 'n';

    std::cin >> answer;

    if(answer == 'y')
    { 
        EvenNumbers(N);
    }
    else
    {
        OddNumbers(N);
    }
    
    return 0;
}